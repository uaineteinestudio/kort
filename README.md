# Kort

A MonoGame/XNA tile engine template.

Contains:

* Tile types
* Camera
* Sprite example
* Simplified movement
* Cell automa caves
* Water generation

## Requires:

MonoGame/XNA

## Installing

##### Building with Windows:

Use Visual Studio to build the project file into an executable, deploy to the working directory of any future project you are making.

## Authors

* **Daniel Stamer-Squair** - *UaineTeine*

## License

This project is licensed under the GNU GPL 3.0 License - see the [LICENSE](LICENSE) file for details.